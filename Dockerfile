ARG UBUNTU_RELEASE=jammy

FROM ubuntu:$UBUNTU_RELEASE

ARG USER_NAME=ci
ARG USER_ID=1000
ARG USER_HOME=/build
ARG UBUNTU_RELEASE # Docker weirdness to retain ARG across FROM

LABEL org.opencontainers.image.authors="David James Sherman <david.sherman@inria.fr>"
LABEL org.opencontainers.image.title="Basic CI build image"
LABEL org.opencontainers.image.description="Ubuntu ${UBUNTU_RELEASE} image for CI builds with cmake and git"

# Basic tools for compiling

RUN apt-get update && apt-get install -y \
    build-essential \
    libudev-dev \
    cmake \
    git \
    wget \
    && rm -rf /var/lib/apt/lists/*

# Openshift best practices for nonroot containers

RUN useradd --uid ${USER_ID} --groups 0 --create-home ${USER_NAME} --home-dir ${USER_HOME}

RUN chown -R ${USER_ID}:0 ${USER_HOME} &&\
    chmod -R g=u ${USER_HOME} &&\
    find ${USER_HOME} -type d -print0 | xargs -0 chmod g+ws

WORKDIR $USER_HOME
ENV HOME=$USER_HOME
USER $USER_ID
