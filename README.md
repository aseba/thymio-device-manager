# Thymio device manager

Build the Thymio Device Manager as a standalone application, without
the rest of Thymio Suite.

```sh
git clone https://gitlab.inria.fr/aseba/thymio-device-manager.git
cd thymio-device-manager
git submodule update --init
mkdir -p build && cd build
cmake ..
make all -j $[ $(nproc) - 1 ]
```

The `git submodule update` step will clone the submodules, but not
recursively because we don't want the submodules in `aseba`.

The first time `cmake` is run it will download Boost 1.73; this will
take time.

Building in CI relies on a basic Ubuntu image specified by
[`Dockerfile`](Dockerfile).


## Strategy

This project uses a special [`CMakeLists.txt`](CMakeLists.txt)
specification that does not use the CMake rules of the upstream
https://github.com/Mobsya/aseba.git source project. It only compiles
the parts of Boost 1.73 that are needed. Most dependencies, except for
`flatbuffers` and `expected`, are headers-only.

This configuration is probably quite fragile.


## Author

David James Sherman, <david.sherman@inria.fr>
<br/>(C) 2023 Inria


## Acknowledgements

Thanks to Yves Piguet for the heavy lifting of identifying the minimal
dependencies for the Thymio Device Manager.
